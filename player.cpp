#include "player.h"
#include <vector>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

	play = new Board();
	AI_side = side;
	move = NULL;
	if(side == WHITE)
	{
		opponent = BLACK;
	}
	else
	{
		opponent = WHITE;
	}
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	play->doMove(opponentsMove, opponent);	

	//return maybeWin();
	return heuristic();
	//return minimax();
}
Move *Player::maybeWin() {
	if(!play->isDone())
	{
		for(int j = 0; j < 8; j++)
		{
			for(int i = 0; i < 8; i++)
			{
				Move *move = new Move(j, i);
				if(play->checkMove(move, AI_side))
				{
					play->doMove(move, AI_side);
					return move;
				}
			}
		}
	}		
    return NULL;
}

Move *Player::heuristic() {
	if(!play->isDone())
	{		
		int highest_score = -1000000;
		Move *best_move = NULL; 
	
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				Move *move = new Move(i, j);
				if(play->checkMove(move, AI_side))
				{
					int temp_score = play->score(move, AI_side);
					if(temp_score > highest_score)
					{
						highest_score = temp_score;
						if(best_move !=NULL)
						{							
							delete best_move;
						}						
						best_move = new Move(i, j);
					}
				}
				delete move;
			}
		}
		play->doMove(best_move, AI_side);
		return best_move;
	}

	return NULL;	
}


vector<Move*> Player::getFirstMoves(Side side) {
	
	Move *good_moves = NULL;
	vector<Move*> child;
	if(!play->isDone())
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				Move* move = new Move(i, j);
				if(play->checkMove(move, side))
				{
					if(good_moves != NULL)
					{
						delete good_moves;		
					}					
					good_moves = new Move(i, j);
					child.push_back(good_moves);
				}
				delete move;
			}
		}							
	}

	return child;
}
