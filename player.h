#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <algorithm>

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	Move *maybeWin();
	Move *heuristic();
	int minimax(Move *move, int depth, Side side);
	int alphaBeta(Move *move, int depth, Side side);

	vector<Move*> getFirstMoves(Side side);
	
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
	Board *play;
	Side AI_side;
	Side opponent;	
	Move *move;
	
	
};

#endif
